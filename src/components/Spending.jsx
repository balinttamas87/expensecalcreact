import firebase from "firebase";
import React from "react";
import "./Spending.css";
import getDiffOfDates from "../helpers/getDiffOfDates";
import getTodaysDate from "../helpers/getTodaysDate";
import getDaysInCurrentMonth from "../helpers/getDaysInCurrentMonth";
import getTimestamps from "../helpers/getTimestamps";

// Initialize Firebase
const config = {
  apiKey: "AIzaSyAPN0C7fZK9DJ4iSwuGGjjpKMrfduyLygM",
  authDomain: "expensecalcreact.firebaseapp.com",
  databaseURL: "https://expensecalcreact.firebaseio.com",
  storageBucket: "expensecalcreact.appspot.com",
  messagingSenderId: "96236638930",
  projectId: "expensecalcreact"
};

firebase.initializeApp(config);

const db = firebase.firestore();

const usersCollection = db.collection("users");

const userDocument = usersCollection.doc("tamas");

const userPurchasesCollection = userDocument.collection("purchases");

class Spending extends React.Component {
  constructor() {
    super();

    this.state = {
      totalSpent: 0,
      averageDailySpending: 0,
      suggestedDailySpendingLimit: null,
      listOfSpendings: []
    };
    
  }

  input = React.createRef();
  limitInput = React.createRef();
  titleOfPurchaseInput = React.createRef();

  componentDidMount() {
    this.getData();
  }

  getData = () => {
    const {monthStart, monthEnd} = getTimestamps();
    userPurchasesCollection
      .where("timestamp", ">", monthStart)
      .where("timestamp", "<", monthEnd)
      .get()
      .then((snap) => {
      
        const listOfSpendings = snap.docs.map(doc => {
          const  id = doc.id;
          const { title, value, timestamp } = doc.data();
          console.log("doc:", doc);
          return { title, value, id, timestamp }
        });
        const totalSpent = listOfSpendings.reduce((total, purchase) => {
          total += parseFloat(purchase.value);
          return total;
        }, 0);

        this.setState({
          totalSpent: totalSpent,
          averageDailySpending: this.getAverageDailySpending(totalSpent),
          suggestedDailySpendingLimit: this.getMaxSuggestedDailyLimit(totalSpent),
          listOfSpendings: listOfSpendings
        });

    });

  }

  handleSubmit = e => {

    e.preventDefault();
    this.addToTotal();
    this.input.current.value = null;
    this.titleOfPurchaseInput.current.value = null;

  };

  addToTotal = () => {
    this.input.current.blur();
    const inputValue = this.input.current.value;
    const parsedValue = parseFloat(inputValue);
    const titleOfPurchaseInput = this.titleOfPurchaseInput.current.value;
    const isValidNumber =
      typeof parsedValue === "number" && !isNaN(parsedValue) && parsedValue > 0;

    if (isValidNumber) {
      const totalSpent = this.state.totalSpent + parseFloat(this.input.current.value);

      const timestamp = new Date().getTime();
    
      userPurchasesCollection.add({
        title: titleOfPurchaseInput,
        value: inputValue,
        timestamp: timestamp
      })
      .then((docRef) => {
        console.log(docRef.id);
        this.setState({
          listOfSpendings: [
            ...this.state.listOfSpendings,
            {
              title: titleOfPurchaseInput, value: inputValue, id: docRef.id, timestamp: timestamp
            }
          ],
          totalSpent: totalSpent,
          averageDailySpending: this.getAverageDailySpending(totalSpent),
          suggestedDailySpendingLimit: this.getMaxSuggestedDailyLimit(totalSpent)
        });
      });
    }
  };

  getAverageDailySpending = total => {
    const averageDailySpending = total / new Date().getDate();
    return parseFloat(averageDailySpending.toFixed(2));
  };

  getMaxSuggestedDailyLimit = (totalSpent) => {
    const currentYear = new Date(Date.now()).getFullYear();
    const currentMonth = new Date(Date.now()).getMonth();

    const monthlySpendingLimit = parseFloat(this.limitInput.current.value) || 300;
    const dailyLimit = (monthlySpendingLimit - totalSpent) / (getDiffOfDates(getTodaysDate(), getDaysInCurrentMonth(currentYear, currentMonth)));
    return parseFloat(dailyLimit.toFixed(2));

  }

  handleDelete = (id) => {

    userPurchasesCollection.doc(`${id}`).delete()
      .then(() => {
        console.log("deleted:", id);
        this.getData();
      })
      .catch((error) => {
        alert("Error deleting purchase");
      })
  }
  
  render() {
    return (
      <React.Fragment>
        <div className="limitPeriodWrapper">
          <label htmlFor="monthlySpendingLimit">Spending limit (default: £300)</label>
          <input ref={this.limitInput} type="number" id="monthlySpendingLimit" />
        </div>
        <div className="main">
          <div>
            Limit period: <span id="limitPeriodElement" /> (default: montly)
          </div>
          <div>
          {/*{Suggested maximum daily spending limit to not to exceed montly
            limit:}*/}
             Suggested daily spending:<span id="suggestedMaxDailyLimit" /> £{this.state.suggestedDailySpendingLimit}
          </div>
          <div id="totalValueOuter">
            Total spent:{" "}
            <span id="totalValueInner">£{this.state.totalSpent}</span>
          </div>
          <div id="avgDailySpendingOuter">
            Average daily spending:{" "}
            <span id="avgDailySpendingInner">
              £{this.state.averageDailySpending}
            </span>
          </div>
          <form action="" onSubmit={this.handleSubmit}>
            <div id="valueInputWrapper">
              <input ref={this.titleOfPurchaseInput} type="text" id="titleOfPurchaseInput" />
              <input ref={this.input} type="number" id="addToTotalInput" />
              <button onClick={this.handleSubmit}type="submit" id="submitBtn">
                Add to Total
              </button>
            </div>
          </form>
          <div>
            <ul className="listOfSpendings">
              {
                this.state.listOfSpendings && this.state.listOfSpendings.map((value, index) => (
                  <React.Fragment key={`${value.value}_${index}`}>
                    <li>
                      <span className="spending-title">{value.title}:</span>
                      <span className="spending-value">£{value.value}</span>
                      <button onClick={() => this.handleDelete(value.id)} className="spending-delete">X</button>
                    </li>
                  </React.Fragment>
                ))
            }
            </ul>
          </div>
        </div>
      </React.Fragment>
    );
  }
}

export default Spending;