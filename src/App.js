import React, { Component } from 'react';
import './App.css';
import Spending from "./components/Spending";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Spending />
      </div>
    );
  }
}

export default App;
