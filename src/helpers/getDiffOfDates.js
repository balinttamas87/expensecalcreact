export default function getDiffOfDates(currentDate, lastDateOfTheMonth) {
    return (lastDateOfTheMonth - currentDate);
}