export default () => {
	const date = new Date();

	const monthStart = new Date(
		date.getFullYear(), date.getMonth(), 1
	).getTime();

	const dayInUnixTime = 1000 * 60 * 60 * 24;

	const monthEnd = new Date(
		date.getFullYear(), date.getMonth() + 1, 0
	).getTime() + dayInUnixTime;

	return {monthStart, monthEnd}
}