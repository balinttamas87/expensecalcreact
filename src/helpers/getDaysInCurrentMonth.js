// month is zero based
export default function getDaysInCurrentMonth(year, month) {

    const lastDateOfPreviousMonth = 0;

    let numOfdaysInCurrentMonth = new Date(year, (month + 1), lastDateOfPreviousMonth).getDate();

    return numOfdaysInCurrentMonth;
}